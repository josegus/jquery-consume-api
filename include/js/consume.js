$(document).ready(function() {
	consume();
});

function consume() {
	var uriString = "https://restcountries.eu/rest/v2/regionalbloc/usan";
  
  	$.getJSON( uriString )
  		.done(function( data ) {
    		appendOnTable(data);
      		loadDatatable(data);
  	});
}

function appendOnTable(data) {
	var content = '';
	$.each( data, function( i, countrie ) {
		var row = '<tr>';
		row += '<td><img class="countrie-flag" src="' + countrie.flag + '" alt="flag.jpg"></td>';
		row += '<td>' + countrie.name + '</td>';
		row += '<td>' + countrie.capital + '</td>';
		row += '<td>' + countrie.languages[0].nativeName + '</td>';
		row += '<td>' + countrie.population + '</td>';
		row += '</tr>';

		content += row;
	});
	$(".countries tbody").append(content);
}

function loadDatatable(dataSet) {
	var table = $('#datatable');
	/*var uriString = "https://restcountries.eu/rest/v2/regionalbloc/usan";*/
    table.DataTable({      
        /*ajax: {
            url: uriString,
            type: 'get',
            dataSrc: ''
        },*/
        data: dataSet,
        columns: [
            { data: 'flag' },
            { data: 'name' },
            { data: 'capital' },
            { data: 'language' },
            { data: 'population' },
        ],
        columnDefs: [{
            targets: 0,
            visible: true,
            searchable: false,
            sortable: false,
            render: function(data, type, row) {            	
                return '<img class="countrie-flag" src="' + row.flag + '" alt="flag.jpg">';
            }
        }, {
        	targets: 3,
        	render: function(data, type, row) {
        		return row.languages[0].nativeName;
        	}
        }],
        responsive: true,
        pagingType: "full_numbers"       
    });
}