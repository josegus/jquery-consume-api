$(document).ready(function() {
	var table = $('#datatable');
	var uriString = "https://restcountries.eu/rest/v2/regionalbloc/usan";
    table.DataTable({      
        ajax: {
            url: uriString,
            type: 'get',
            dataSrc: ''
        },
        columns: [
            { data: 'flag' },
            { data: 'name' },
            { data: 'capital' },
            { data: 'language' },
            { data: 'population' },
        ],
        columnDefs: [{
            targets: 0,
            visible: true,
            searchable: false,
            sortable: false,
            render: function(data, type, row) {            	
                return '<img class="countrie-flag" src="' + row.flag + '" alt="flag.jpg">';
            }
        }, {
        	targets: 3,
        	render: function(data, type, row) {
        		return row.languages[0].nativeName;
        	}
        }],
        responsive: true,
        pagingType: "full_numbers"       
    });
});